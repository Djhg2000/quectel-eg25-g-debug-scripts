# Debugging scripts for Quectel EG25-G

Use with caution. These scripts are intended for manual debugging of a Quectel EG25-G modem on a pinephone and assumes you know what youre doing.

## pinephone-modem-start.sh

Contains pin setup and the power on sequence.

## pinephone-modem-stop.sh

Contains the power off sequence.

## pinephone-modem-reset.sh

Contains the reset sequence.

## pinephone-modem-diagnostics.sh

Basic diagnostics script, doesn't do much (yet)

