#!/bin/sh

# GPIO68 is RESET_N
# Sequence requires RESET_N to be pulled low for 150-460 ms

# Configure outputs
for i in 68
do
        echo "Setting GPIO$i\tas output"
        [ -e /sys/class/gpio/gpio$i ] && continue
        echo $i > /sys/class/gpio/export || return 1
        echo out > /sys/class/gpio/gpio$i/direction || return 1
done

echo "Resetting EG25 WWAN module"
echo "Setting RESET_N low" # Inverted
echo 1 > /sys/class/gpio/gpio68/value
sleep 0.15 # Guarantees at least 150 ms
echo "Setting RESET_N high" # Inverted
echo 0 > /sys/class/gpio/gpio68/value

# Deconfigure outputs
for i in 68
do
        echo "Unexporting GPIO$i"
        [ ! -e /sys/class/gpio/gpio$i ] && continue
        echo $i > /sys/class/gpio/unexport || return 1
done

