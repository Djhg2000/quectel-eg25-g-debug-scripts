#!/bin/bash
# Bash is required for read timeouts and other bash-specific features
# Do not report bugs if you force another shell

# GPIO35 is PWRKEY
# GPIO68 is RESET_N
# GPIO231 is WAKEUP_IN on BH and AP_READY (active low) on CE
# GPIO232 is W_DISABLE#
# GPIO233 is STATUS

# DTR is:
# - PL6/GPIO358 on BH (1.1)
# - PB2/GPIO34 on CE (1.2)

TTY_UART="/dev/ttyS2"
TTY_USB="/dev/ttyUSB2"



### Function definitions ###

# builtin_atinout <TTY> <SEND>
# returns <RECV>
# Hacky function to replace atinout
# Basically a structured set of race conditions, but it works
builtin_atinout () {
	local RECV
	(sleep 0.1; printf "$2\r\n" > $1) &
	read -t 1 -d '' RECV < $1
	echo "$RECV"
}



# Global state vars
status_GPIO=""
status_TTY=""



# Detect PinePhone version
if grep -q 1.1 /proc/device-tree/model ; then
	printf "Detected PinePhone v1.1\n"
	printf "WAKEUP_IN will be mislabled as AP_READY\n"
	DTR=358
elif grep -q 1.2 /proc/device-tree/model ; then
	printf "Detected PinePhone v1.2(a/b)\n"
	DTR=34
else
	printf "Failed to detect PinePhone version!\n"
	exit 1
fi



# Only set up GPIO if eg25manager process isn't running
if ! (pidof -q eg25manager) ; then

	printf "\n### GPIO configuration ###\n"
	# Configure outputs
	for i in 35 68 231 232 $DTR
	do
		printf "Configuring GPIO$i\tas output\n"
		[ -e /sys/class/gpio/gpio$i ] && continue
		printf "$i" > /sys/class/gpio/export || return 1
		printf "out" > /sys/class/gpio/gpio$i/direction || return 1
	done

	# Configure inputs
	for j in 233
	do
		printf "Configuring GPIO$j\tas input\n"
		[ -e /sys/class/gpio/gpio$j ] && continue
		printf "$j" > /sys/class/gpio/export || return 1
		printf "in" > /sys/class/gpio/gpio$j/direction || return 1
	done



	# Detect expected power state
	printf "\n### POWER ###\n"
	printf "Detecting expected power state:\n"

	state_RESET_N=$(cat /sys/class/gpio/gpio68/value)
	if [ "$state_RESET_N" -eq "0" ] ; then
		printf " RESET_N     high (not held in reset)\n" # Inverted
	else
		printf " RESET_N     low  (held in reset)\n"
	fi

	state_AP_READY=$(cat /sys/class/gpio/gpio231/value)
	if [ "$state_AP_READY" -eq "0" ] ; then
		printf " AP_READY    low  (wakeup)\n"
	else
		printf " AP_READY    high (sleep)\n"
	fi

	state_W_DISABLE=$(cat /sys/class/gpio/gpio232/value)
	if [ "$state_W_DISABLE" -eq "0" ] ; then
		printf " W_DISABLE#  high (wireless enabled)\n" # Inverted
	else
		printf " W_DISABLE#  low  (wireless disabled)\n"
	fi

	state_DTR=$(cat /sys/class/gpio/gpio$DTR/value)
	if [ "$state_DTR" -eq "0" ] ; then
		printf " DTR         low  (serial enabled)\n"
	else
		printf " DTR         high (serial disabled)\n"
	fi

	state_STATUS=$(cat /sys/class/gpio/gpio233/value)
	if [ "$state_STATUS" -eq "0" ] ; then
		printf " STATUS      low  (modem idle)\n"
	else
		printf " STATUS      high (modem busy?)\n"
	fi

	# Report state to the user
	if [ "$state_RESET_N" -eq "0" ] && \
			[ "$state_AP_READY" -eq "0" ] && \
			[ "$state_W_DISABLE" -eq "0" ] && \
			[ "$state_DTR" -eq "0" ] && \
			[ "$state_STATUS" -eq 0 ] ; then
		printf "Modem is expected to be ON and reports ON\n"

	elif [ "$state_RESET_N" -eq "0" ] && \
			[ "$state_AP_READY" -eq "0" ] && \
			[ "$state_W_DISABLE" -eq "0" ] && \
			[ "$state_DTR" -eq "0" ] && \
			[ "$state_STATUS" -eq 1 ] ; then
		printf "Modem is expected to be ON but reports OFF\n"

	elif [ "$state_STATUS" -eq "0" ] ; then

		printf "Modem is expected to be OFF but reports ON\n"
		status_GPIO="$status_GPIO I"
	else
		printf "Modem is expected to be OFF and reports OFF\n"
		status_GPIO="$status_GPIO I"
	fi

	printf "\n### GPIO deconfiguration ###\n"
	# Deconfigure outputs
	for i in 35 68 231 232 $DTR
	do
	        printf "Deconfiguring GPIO$i\n"
	        [ ! -e /sys/class/gpio/gpio$i ] && continue
	        printf $i > /sys/class/gpio/unexport || return 1
	done

	# Deconfigure inputs
	for j in 233
	do
	        printf "Deconfiguring GPIO$j\n"
	        [ ! -e /sys/class/gpio/gpio$j ] && continue
	        printf $j > /sys/class/gpio/unexport || return 1
	done

else
	# If we get here we couldn't claim the GPIO because eg25manager is running
	printf "### Process eg25manager is running, skipping GPIO setup ###\n"
	status_GPIO="$status_GPIO O"
fi



printf "\n### COMMUNICATION ###\n"
for TTY in "$TTY_UART" "$TTY_USB"
do
	if [ "$TTY" == "$TTY_UART" ] ; then
		TTY_NAME="UART"
	elif [ "$TTY" == "$TTY_USB" ] ; then
		TTY_NAME="USB"
	else
		TTY_NAME="UNKNOWN"
	fi

	printf "Attempting to contact modem over $TTY_NAME:\n"
	
	# Check if device exists
	if [ ! -e "$TTY" ] ; then
		printf "TTY device for $TTY_NAME not found\n"
		status_TTY="$status_TTY ${TTY_NAME}D"
		continue
	fi

	printf "Configuring $TTY_NAME\n"
	TTY_CFG="$(stty -F "$TTY" -g)"
	# Disable local echo
	stty -F "$TTY" -echo 2>&1 > /dev/null
	if [ $? != 0 ] ; then
		printf "Failed to configure $TTY_NAME, TTY is busy"
		status_TTY="$status_TTY ${TTY_NAME}B"
		break
	fi
	for AT_CMD in "AT" "ATI"
	do
		printf " $AT_CMD\t"
		RESPONSE="$(builtin_atinout /dev/ttyS2 "$AT_CMD")"
		RESPONSE_HEX="$(xxd -p <<< "$RESPONSE")"
		if [ -z "$RESPONSE" ] ; then
			printf "No response\n"
			status_TTY="$status_TTY ${TTY_NAME}N"
			break
		# Response is matched with wildcard because sometimes the modem echoes
		elif [ "$AT_CMD" == "AT" ] && \
			[[ "$RESPONSE_HEX" == *"4f4b0a" ]] \
			; then
			printf "OK\n"
		elif [ "$AT_CMD" == "ATI" ] && \
			[[ "$RESPONSE_HEX" == *"5175656374656c0a0a45473235"*"4f4b0a" ]] \
			; then
			printf "OK\n"
		else
			printf "Invalid response\n"
			status_TTY="$status_TTY ${TTY_NAME}I"
		fi
		printf "$(sed 's/^/\t|/' <<< "$(xxd <<< "$RESPONSE")")\n"
	done
	printf "Restoring UART config\n"
	stty -F "$TTY" "$TTY_CFG"
done

printf "$status_TTY\n"

# Print summary
printf "\n### Summary ###\n"
printf "GPIO..."
if [ "$status_GPIO" == "" ] ; then
	printf "OK\n"
else
	if [[ "$status_GPIO" == *"I"* ]] ; then
		printf "Invalid state\n"
	elif [[ "$status_GPIO" == *"O"* ]] ; then
		printf "Omitted (eg25-manager is running)\n"
	else
		printf "UNKNOWN ERROR (report bug and paste status_GPIO=\""$status_GPIO"\")\n"
	fi
fi
printf "UART..."
if [[ "$status_TTY" != *"UART"* ]] ; then
	printf "OK\n"
else
	if [[ "$status_TTY" == *"UARTN"* ]] ; then
		printf "No resonse\n"
	elif [[ "$status_TTY" == *"UARTI"* ]] ; then
		printf "Invalid response (see hex dump above)\n"
	elif [[ "$status_TTY" == *"UARTB"* ]] ; then
		printf "Device is busy\n"
	else
		printf "UNKNOWN ERROR (report bug and paste status_TTY=\""$status_TTY"\")\n"
	fi
fi
printf "USB...."
if [[ "$status_TTY" != *"USB"* ]] ; then
	printf "OK\n"
else
	if [[ "$status_TTY" == *"USBN"* ]] ; then
		printf "No resonse\n"
	elif [[ "$status_TTY" == *"USBI"* ]] ; then
		printf "Garbage output (see hex dump above)\n"
	elif [[ "$status_TTY" == *"USBD"* ]] ; then
		printf "Device not found\n"
	elif [[ "$status_TTY" == *"USBB"* ]] ; then
		printf "Device is busy\n"
	else
		printf "UNKNOWN ERROR (report bug and paste status_USB=\""$status_TTY"\")\n"
	fi
fi

